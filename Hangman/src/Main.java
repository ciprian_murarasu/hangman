import java.util.Scanner;

public class Main {
    static String[] words = new String[]{"batait", "prijin", "inzeci", "rodozahar", "gargolot", "picingene", "argintareasa", "extompa", "anot", "derocare", "obtuziune", "puntura", "inmatriculare", "ciorb", "esenta", "descatarama", "chiarificatiune", "inticsi", "beglita", "proceda", "iarmaroc", "sinusoida", "programare", "incumetrire", "diplozom", "dadaci", "conostirita", "suplicare", "cliucaitor", "zagan", "strepeziciune", "rotitura", "lezard", "dosala", "dressing", "pentagon", "negativ", "ritoricesc", "viitorologie", "submersiune", "telestereograf", "frap", "galcevit", "scorobaie", "asibilare", "piromantie", "zavastra", "corcoata", "injectabil", "cazanie", "euhalin", "ghe", "bronchita", "uilier", "inactiune", "insubordinatie", "despedecat", "tambura", "badiulut", "iglite", "ciclonic", "cambriolaj", "azmutare", "protuberanta", "folosinta", "aprehensiune", "ingurgare", "corturel", "beregatuit", "hipoacid", "deviza", "iberica", "bagrin", "tuiac", "rama", "supozitor", "indianist", "stimulatoare", "prustar", "ncasat", "pohoace", "cutrubatura", "usturoia", "componential", "ganasa", "convocat", "dentist", "intraperitoneal", "rasatura", "franciza", "buda", "sufleci", "amforeta", "poticali", "lucoare", "creapla"};
//    static String[] words_diac = new String[]{"bătăit", "prijin", "înzeci", "rodozahar", "gârgoloț", "picingene", "argintăreasă", "extompa", "anot", "derocare", "obtuziune", "puntură", "înmatriculare", "ciorb", "esență", "descătărăma", "chiarificațiune", "înticsi", "begliță", "proceda", "iarmaroc", "sinusoidă", "programare", "încumetrire", "diplozom", "dădăci", "conoștiriță", "suplicare", "cliucăitor", "zagan", "strepeziciune", "rotitură", "lezard", "dosală", "dressing", "pentagon", "negativ", "ritoricesc", "viitorologie", "submersiune", "telestereograf", "frap", "gâlcevit", "scorobaie", "asibilare", "piromanție", "zăvastră", "corcoață", "injectabil", "cazanie", "euhalin", "ghe", "bronchită", "uilier", "inacțiune", "insubordinație", "despedecat", "tambură", "bădiuluț", "iglițe", "ciclonic", "cambriolaj", "azmuțare", "protuberanță", "folosință", "aprehensiune", "îngurgare", "corturel", "beregătuit", "hipoacid", "deviză", "iberică", "bagrin", "tuiac", "ramă", "supozitor", "indianist", "stimulatoare", "prustar", "ncasat", "pohoace", "cutrubătură", "usturoia", "componențial", "ganașă", "convocat", "dentist", "intraperitoneal", "răsătură", "franciză", "buda", "sufleci", "amforetă", "poticăli", "lucoare", "creaplă"};

    static String chosenWord; // Aleg un cuvant din cele disponibile
    static char[] chosenWordLetters; // Impart cuvantul pe litere
    static char[] letters; // Salvez literele gasite
    static char[] usedLetters; // Salvez literele folosite pentru comparari ulterioare
    static boolean[] wrongLetters; // Tin minte daca o litera introdusa a fost gresita sau nu

    static Scanner scan = new Scanner(System.in);

    public static void main(String[] args) {
        boolean newGame = true;
        while (newGame) {
            resetGame();
            for (int i = 0; i < letters.length; i++) { // Initializez array-ul pentru literele gasite
                letters[i] = '_';
            }
            int countLettersLeft = letters.length; // Numar cate litere au mai ramas de completat
            int noOfFails = 0; // Numar literele gresite unice
            boolean win;

            while (noOfFails <= 6 && countLettersLeft > 0) {
                showLetters();
                if (noOfFails > 0) {
                    showWrongLetters();
                }
                System.out.print("Introduceti litera: ");
                char newLetter = getNewLetter();
                int letterIndex = (int) newLetter - 97;
                while (usedLetters[letterIndex] == newLetter) {
//                if (wrongLetters[letterIndex] == false) {
                    showLetters();
//                }
                    if (noOfFails > 0)
                        showWrongLetters();
                    System.out.print("Litera deja introdusa. Alegeti alta litera: ");
                    newLetter = getNewLetter();
                    letterIndex = (int) newLetter - 97;
                }
                usedLetters[letterIndex] = newLetter;
                // Verific daca litera introdusa se afla in cuvant
                boolean fail = true;
                for (int i = 0; i < chosenWordLetters.length; i++) {
                    if (chosenWordLetters[i] == newLetter) {
                        letters[i] = newLetter;
                        countLettersLeft--;
                        fail = false;
                        if (i == 0) {
                            letters[i] -= 32;
                        }
                    }
                }
                if (fail && wrongLetters[letterIndex] == false) {
                    wrongLetters[letterIndex] = true;
                    noOfFails++;
                }
            }
            if (noOfFails > 6) {
                if (countLettersLeft > 0) {
                    win = false;
                    showWrongLetters();
                    System.out.println("Ai fost facut girafa! Cuvantul era " + chosenWord.toUpperCase());
                }
            } else {
                win = true;
                showLetters();
                System.out.println("Ai castigat!");
            }
            System.out.print("Joc nou? (da/nu) ");
            if (!scan.nextLine().equals("da"))
                newGame = false;
        }
    }

    static void resetGame() {
        chosenWord = words[(int) (Math.floor(Math.random() * words.length))];
//        chosenWord = words[12]; // for test only
        chosenWordLetters = chosenWord.toCharArray();
        letters = new char[chosenWord.length()];
        usedLetters = new char[26];
        wrongLetters = new boolean[26];
    }

    static void showLetters() {
        for (int i = 0; i < letters.length; i++) {
            System.out.print(letters[i] + " ");
        }
        System.out.println();
    }

    static void showWrongLetters() {
        System.out.print("Litere gresite: ");
        for (int j = 0; j < usedLetters.length; j++) {
            if (wrongLetters[j] == true) {
                System.out.print(usedLetters[j] + " ");
            }
        }
        System.out.println();
    }

    static char getNewLetter() {
        String input = scan.nextLine();
        while (!input.matches("[a-zA-Z]{1}")) {
            System.out.print("Incorect. Alta litera: ");
            input = scan.nextLine();
        }
        return input.toLowerCase().charAt(0);
    }
}
